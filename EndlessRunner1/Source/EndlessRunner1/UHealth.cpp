// Fill out your copyright notice in the Description page of Project Settings.

#include "UHealth.h"

// Sets default values for this component's properties
UUHealth::UUHealth()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UUHealth::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UUHealth::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UUHealth::GetDamage(float damage)
{
	mDamage = damage;
}

void UUHealth::TakeDamage(float damage)
{
	if (damage < 0) return;	
	mCurrentHealth -= damage;
	if (mCurrentHealth < 0) mCurrentHealth = 0;
	//if (mCurrentHealth == 0) OnDeath();
}

float UUHealth::GetNormalizedHP()
{
	return ( mCurrentHealth / mMaxHealth ) * 100;
}

void UUHealth::Heal(float value)
{
	mCurrentHealth += value
}

void UUHealth::Restore()
{
	mCurrentHealth = mMaxHealth;
}




