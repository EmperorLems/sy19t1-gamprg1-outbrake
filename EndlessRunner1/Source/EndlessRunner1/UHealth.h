// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "UHealth.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ENDLESSRUNNER1_API UUHealth : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UUHealth();
	UPROPERTY(BlueprintReadWrite, EditAnywhere)	
	float mCurrentHealth;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float mMaxHealth;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float mDamage;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


public:
	UFUNCTION(BlueprintCallable)
	void GetDamage(float damage);

	UFUNCTION(BlueprintCallable)
	void TakeDamage(float damage);

	UFUNCTION(BlueprintPure)
	float GetNormalizedHP();

	UFUNCTION(BlueprintCallable)
	void Heal(float value);

	UFUNCTION(BlueprintCallable)
	void Restore();

};
