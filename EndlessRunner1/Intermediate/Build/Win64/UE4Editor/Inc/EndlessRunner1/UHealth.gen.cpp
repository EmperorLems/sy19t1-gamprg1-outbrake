// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EndlessRunner1/UHealth.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUHealth() {}
// Cross Module References
	ENDLESSRUNNER1_API UClass* Z_Construct_UClass_UUHealth_NoRegister();
	ENDLESSRUNNER1_API UClass* Z_Construct_UClass_UUHealth();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_EndlessRunner1();
	ENDLESSRUNNER1_API UFunction* Z_Construct_UFunction_UUHealth_GetDamage();
	ENDLESSRUNNER1_API UFunction* Z_Construct_UFunction_UUHealth_GetNormalizedHP();
	ENDLESSRUNNER1_API UFunction* Z_Construct_UFunction_UUHealth_Heal();
	ENDLESSRUNNER1_API UFunction* Z_Construct_UFunction_UUHealth_Restore();
	ENDLESSRUNNER1_API UFunction* Z_Construct_UFunction_UUHealth_TakeDamage();
// End Cross Module References
	void UUHealth::StaticRegisterNativesUUHealth()
	{
		UClass* Class = UUHealth::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetDamage", &UUHealth::execGetDamage },
			{ "GetNormalizedHP", &UUHealth::execGetNormalizedHP },
			{ "Heal", &UUHealth::execHeal },
			{ "Restore", &UUHealth::execRestore },
			{ "TakeDamage", &UUHealth::execTakeDamage },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UUHealth_GetDamage_Statics
	{
		struct UHealth_eventGetDamage_Parms
		{
			float damage;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_damage;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UUHealth_GetDamage_Statics::NewProp_damage = { UE4CodeGen_Private::EPropertyClass::Float, "damage", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(UHealth_eventGetDamage_Parms, damage), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUHealth_GetDamage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUHealth_GetDamage_Statics::NewProp_damage,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUHealth_GetDamage_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UHealth.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUHealth_GetDamage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUHealth, "GetDamage", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(UHealth_eventGetDamage_Parms), Z_Construct_UFunction_UUHealth_GetDamage_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UUHealth_GetDamage_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUHealth_GetDamage_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UUHealth_GetDamage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUHealth_GetDamage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUHealth_GetDamage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUHealth_GetNormalizedHP_Statics
	{
		struct UHealth_eventGetNormalizedHP_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UUHealth_GetNormalizedHP_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(UHealth_eventGetNormalizedHP_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUHealth_GetNormalizedHP_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUHealth_GetNormalizedHP_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUHealth_GetNormalizedHP_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UHealth.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUHealth_GetNormalizedHP_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUHealth, "GetNormalizedHP", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14020401, sizeof(UHealth_eventGetNormalizedHP_Parms), Z_Construct_UFunction_UUHealth_GetNormalizedHP_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UUHealth_GetNormalizedHP_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUHealth_GetNormalizedHP_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UUHealth_GetNormalizedHP_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUHealth_GetNormalizedHP()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUHealth_GetNormalizedHP_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUHealth_Heal_Statics
	{
		struct UHealth_eventHeal_Parms
		{
			float value;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UUHealth_Heal_Statics::NewProp_value = { UE4CodeGen_Private::EPropertyClass::Float, "value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(UHealth_eventHeal_Parms, value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUHealth_Heal_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUHealth_Heal_Statics::NewProp_value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUHealth_Heal_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UHealth.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUHealth_Heal_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUHealth, "Heal", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(UHealth_eventHeal_Parms), Z_Construct_UFunction_UUHealth_Heal_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UUHealth_Heal_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUHealth_Heal_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UUHealth_Heal_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUHealth_Heal()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUHealth_Heal_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUHealth_Restore_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUHealth_Restore_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UHealth.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUHealth_Restore_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUHealth, "Restore", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUHealth_Restore_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UUHealth_Restore_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUHealth_Restore()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUHealth_Restore_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUHealth_TakeDamage_Statics
	{
		struct UHealth_eventTakeDamage_Parms
		{
			float damage;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_damage;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UUHealth_TakeDamage_Statics::NewProp_damage = { UE4CodeGen_Private::EPropertyClass::Float, "damage", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(UHealth_eventTakeDamage_Parms, damage), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUHealth_TakeDamage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUHealth_TakeDamage_Statics::NewProp_damage,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUHealth_TakeDamage_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UHealth.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUHealth_TakeDamage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUHealth, "TakeDamage", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(UHealth_eventTakeDamage_Parms), Z_Construct_UFunction_UUHealth_TakeDamage_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UUHealth_TakeDamage_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUHealth_TakeDamage_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UUHealth_TakeDamage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUHealth_TakeDamage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUHealth_TakeDamage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UUHealth_NoRegister()
	{
		return UUHealth::StaticClass();
	}
	struct Z_Construct_UClass_UUHealth_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_mDamage_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_mDamage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_mMaxHealth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_mMaxHealth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_mCurrentHealth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_mCurrentHealth;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUHealth_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_EndlessRunner1,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UUHealth_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UUHealth_GetDamage, "GetDamage" }, // 3029076556
		{ &Z_Construct_UFunction_UUHealth_GetNormalizedHP, "GetNormalizedHP" }, // 1071929752
		{ &Z_Construct_UFunction_UUHealth_Heal, "Heal" }, // 4237402808
		{ &Z_Construct_UFunction_UUHealth_Restore, "Restore" }, // 476570089
		{ &Z_Construct_UFunction_UUHealth_TakeDamage, "TakeDamage" }, // 4165272789
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUHealth_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "UHealth.h" },
		{ "ModuleRelativePath", "UHealth.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUHealth_Statics::NewProp_mDamage_MetaData[] = {
		{ "Category", "UHealth" },
		{ "ModuleRelativePath", "UHealth.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UUHealth_Statics::NewProp_mDamage = { UE4CodeGen_Private::EPropertyClass::Float, "mDamage", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(UUHealth, mDamage), METADATA_PARAMS(Z_Construct_UClass_UUHealth_Statics::NewProp_mDamage_MetaData, ARRAY_COUNT(Z_Construct_UClass_UUHealth_Statics::NewProp_mDamage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUHealth_Statics::NewProp_mMaxHealth_MetaData[] = {
		{ "Category", "UHealth" },
		{ "ModuleRelativePath", "UHealth.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UUHealth_Statics::NewProp_mMaxHealth = { UE4CodeGen_Private::EPropertyClass::Float, "mMaxHealth", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(UUHealth, mMaxHealth), METADATA_PARAMS(Z_Construct_UClass_UUHealth_Statics::NewProp_mMaxHealth_MetaData, ARRAY_COUNT(Z_Construct_UClass_UUHealth_Statics::NewProp_mMaxHealth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUHealth_Statics::NewProp_mCurrentHealth_MetaData[] = {
		{ "Category", "UHealth" },
		{ "ModuleRelativePath", "UHealth.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UUHealth_Statics::NewProp_mCurrentHealth = { UE4CodeGen_Private::EPropertyClass::Float, "mCurrentHealth", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(UUHealth, mCurrentHealth), METADATA_PARAMS(Z_Construct_UClass_UUHealth_Statics::NewProp_mCurrentHealth_MetaData, ARRAY_COUNT(Z_Construct_UClass_UUHealth_Statics::NewProp_mCurrentHealth_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUHealth_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUHealth_Statics::NewProp_mDamage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUHealth_Statics::NewProp_mMaxHealth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUHealth_Statics::NewProp_mCurrentHealth,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUHealth_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUHealth>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUHealth_Statics::ClassParams = {
		&UUHealth::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x00B000A4u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_UUHealth_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_UUHealth_Statics::PropPointers),
		"Engine",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UUHealth_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UUHealth_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUHealth()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUHealth_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUHealth, 1216412766);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUHealth(Z_Construct_UClass_UUHealth, &UUHealth::StaticClass, TEXT("/Script/EndlessRunner1"), TEXT("UUHealth"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUHealth);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
