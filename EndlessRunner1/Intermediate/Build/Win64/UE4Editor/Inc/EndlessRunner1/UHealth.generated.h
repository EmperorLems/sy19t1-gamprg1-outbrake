// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ENDLESSRUNNER1_UHealth_generated_h
#error "UHealth.generated.h already included, missing '#pragma once' in UHealth.h"
#endif
#define ENDLESSRUNNER1_UHealth_generated_h

#define EndlessRunner1_Source_EndlessRunner1_UHealth_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execRestore) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Restore(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execHeal) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Heal(Z_Param_value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetNormalizedHP) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetNormalizedHP(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTakeDamage) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_damage); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->TakeDamage(Z_Param_damage); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetDamage) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_damage); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GetDamage(Z_Param_damage); \
		P_NATIVE_END; \
	}


#define EndlessRunner1_Source_EndlessRunner1_UHealth_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execRestore) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Restore(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execHeal) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Heal(Z_Param_value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetNormalizedHP) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetNormalizedHP(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTakeDamage) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_damage); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->TakeDamage(Z_Param_damage); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetDamage) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_damage); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GetDamage(Z_Param_damage); \
		P_NATIVE_END; \
	}


#define EndlessRunner1_Source_EndlessRunner1_UHealth_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUHealth(); \
	friend struct Z_Construct_UClass_UUHealth_Statics; \
public: \
	DECLARE_CLASS(UUHealth, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunner1"), NO_API) \
	DECLARE_SERIALIZER(UUHealth)


#define EndlessRunner1_Source_EndlessRunner1_UHealth_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUUHealth(); \
	friend struct Z_Construct_UClass_UUHealth_Statics; \
public: \
	DECLARE_CLASS(UUHealth, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunner1"), NO_API) \
	DECLARE_SERIALIZER(UUHealth)


#define EndlessRunner1_Source_EndlessRunner1_UHealth_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUHealth(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUHealth) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUHealth); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUHealth); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUHealth(UUHealth&&); \
	NO_API UUHealth(const UUHealth&); \
public:


#define EndlessRunner1_Source_EndlessRunner1_UHealth_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUHealth(UUHealth&&); \
	NO_API UUHealth(const UUHealth&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUHealth); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUHealth); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UUHealth)


#define EndlessRunner1_Source_EndlessRunner1_UHealth_h_13_PRIVATE_PROPERTY_OFFSET
#define EndlessRunner1_Source_EndlessRunner1_UHealth_h_10_PROLOG
#define EndlessRunner1_Source_EndlessRunner1_UHealth_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunner1_Source_EndlessRunner1_UHealth_h_13_PRIVATE_PROPERTY_OFFSET \
	EndlessRunner1_Source_EndlessRunner1_UHealth_h_13_RPC_WRAPPERS \
	EndlessRunner1_Source_EndlessRunner1_UHealth_h_13_INCLASS \
	EndlessRunner1_Source_EndlessRunner1_UHealth_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlessRunner1_Source_EndlessRunner1_UHealth_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunner1_Source_EndlessRunner1_UHealth_h_13_PRIVATE_PROPERTY_OFFSET \
	EndlessRunner1_Source_EndlessRunner1_UHealth_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlessRunner1_Source_EndlessRunner1_UHealth_h_13_INCLASS_NO_PURE_DECLS \
	EndlessRunner1_Source_EndlessRunner1_UHealth_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlessRunner1_Source_EndlessRunner1_UHealth_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
